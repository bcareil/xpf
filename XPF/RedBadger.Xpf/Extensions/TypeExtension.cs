﻿using System;
using System.Reflection;

namespace RedBadger.Xpf.Extensions
{

#if NETFX_CORE
    
    public static class TypeExtension
    {

        public static bool IsAssignableFrom(this Type type, Type othType)
        {
            return type.GetTypeInfo().IsAssignableFrom(othType.GetTypeInfo());
        }

    }

#endif

}
