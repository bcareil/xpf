#region License
/* The MIT License
 *
 * Copyright (c) 2011 Red Badger Consulting
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
*/
#endregion

namespace RedBadger.Xpf.Adapters.Xna.Input
{
    using System;
    using System.Collections.Generic;
    using System.Reactive.Subjects;
    using Microsoft.Xna.Framework.Input;
    using Microsoft.Xna.Framework.Input.Touch;
    using RedBadger.Xpf.Input;
    using GestureType = Microsoft.Xna.Framework.Input.Touch.GestureType;

    public class InputManager : IInputManager
    {
        private readonly Subject<Gesture> gestures = new Subject<Gesture>();

        private DateTime startDateTime;
        private MouseState previousState;
        private bool didMouseFreeDrag;

        public InputManager()
        {
            this.startDateTime = DateTime.Now;
            this.didMouseFreeDrag = false;

            TouchPanel.EnabledGestures |= GestureType.FreeDrag;
            TouchPanel.EnabledGestures |= GestureType.DragComplete;
            TouchPanel.EnabledGestures |= GestureType.Tap;
        }

        public IObservable<Gesture> Gestures
        {
            get
            {
                return this.gestures;
            }
        }

        public void Update()
        {
            HandleTouchInputs();
#if !WINDOWS_PHONE
            HandleMouseInputs();
#endif
        }

        private void HandleTouchInputs()
        {
            // handle touch panel events
            while (TouchPanel.IsGestureAvailable)
            {
                GestureSample gesture = TouchPanel.ReadGesture();
                switch (gesture.GestureType)
                {
                    case GestureType.FreeDrag:
                        this.gestures.OnNext(
                            new Gesture(
                                Xpf.Input.GestureType.FreeDrag,
                                new Point(gesture.Position.X, gesture.Position.Y),
                                new Vector(gesture.Delta.X, gesture.Delta.Y),
                                gesture.Timestamp
                                ));
                        break;
                    case GestureType.DragComplete:
                        this.gestures.OnNext(
                            new Gesture(
                                Xpf.Input.GestureType.DragComplete,
                                new Point(gesture.Position.X, gesture.Position.Y),
                                Vector.Zero,
                                gesture.Timestamp
                                ));
                        break;
                    case GestureType.Tap:
                        // on tap, emulate a left mouse button click
                        this.gestures.OnNext(
                            new Gesture(
                                Xpf.Input.GestureType.LeftButtonDown,
                                new Point(gesture.Position.X, gesture.Position.Y),
                                new Vector(gesture.Delta.X, gesture.Delta.Y),
                                gesture.Timestamp
                                ));
                        this.gestures.OnNext(
                            new Gesture(
                                Xpf.Input.GestureType.LeftButtonUp,
                                new Point(gesture.Position.X, gesture.Position.Y),
                                new Vector(gesture.Delta.X, gesture.Delta.Y),
                                gesture.Timestamp
                                ));
                        break;
                }
            }
        }

        // NOTE: this section is disabled on WP7 to avoid the mouse
        //       emulation to duplicate click/tap/drag events.
#if !WINDOWS_PHONE
        private void HandleMouseInputs()
        {
            TimeSpan totalTimeSpan = DateTime.Now - this.startDateTime;
            MouseState currentState = Mouse.GetState();
            // handle left mouse button pressed event
            if (this.previousState.LeftButton == ButtonState.Released && currentState.LeftButton == ButtonState.Pressed)
            {
                this.gestures.OnNext(
                    new Gesture(
                        Xpf.Input.GestureType.LeftButtonDown,
                        new Point(currentState.X, currentState.Y),
                        Vector.Zero,
                        totalTimeSpan
                        ));
            }
            // handle left mouse button released event
            if (this.previousState.LeftButton == ButtonState.Pressed &&
                     currentState.LeftButton == ButtonState.Released)
            {
                // if the mouse moved while the left button was down
                // and so, did a "free drag"
                if (this.didMouseFreeDrag)
                {
                    // notify the free drag is now complete
                    didMouseFreeDrag = false;
                    this.gestures.OnNext(
                        new Gesture(
                            Xpf.Input.GestureType.DragComplete,
                            new Point(currentState.X, currentState.Y),
                            Vector.Zero,
                            totalTimeSpan
                            ));
                }
                // finaly, notify the mouse button has been released
                this.gestures.OnNext(
                    new Gesture(
                        Xpf.Input.GestureType.LeftButtonUp,
                        new Point(currentState.X, currentState.Y),
                        Vector.Zero,
                        totalTimeSpan
                        ));
            }
            // handle mouse movement
            if (currentState.X != this.previousState.X || currentState.Y != this.previousState.Y)
            {
                // send the mouse move event regardless whether a button is down
                this.gestures.OnNext(
                    new Gesture(
                        Xpf.Input.GestureType.Move,
                        new Point(currentState.X, currentState.Y),
                        Vector.Zero,
                        totalTimeSpan
                        ));
                // if the left mouse button is held down
                if (this.previousState.LeftButton == ButtonState.Pressed &&
                    currentState.LeftButton == ButtonState.Pressed)
                {
                    // emit the free drag event, for the mouse
                    this.didMouseFreeDrag = true;
                    this.gestures.OnNext(
                        new Gesture(
                            Xpf.Input.GestureType.FreeDrag, 
                            new Point(currentState.X, currentState.Y), 
                            new Vector(currentState.X - this.previousState.X, currentState.Y - this.previousState.Y),
                            totalTimeSpan
                            ));
                }
            }
            //
            this.previousState = currentState;
        }
#endif

    }
}
